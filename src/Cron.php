<?php
namespace Esc\Billing;

class Cron {
    public static function addSchedules($schedule) {
        $schedule->command('esc_billing:check_active')
                 ->hourly();
        $schedule->command('esc_billing:update_usages')
                 ->hourly();
    }
}