<?php
namespace Esc\Billing\Providers;

use Illuminate\Support\ServiceProvider;

use Esc\Shopify\API;


class BillingServiceProvider extends ServiceProvider {
    protected $commands = [
        \Esc\Billing\Console\Commands\CheckActivePlans::class,
    ];
    
    
    public function register() {
        $this->commands($this->commands);
    }

    public function boot() {
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/../Http/routes.php';
        }
        
        $this->publishes([
            __DIR__.'/../database/migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
