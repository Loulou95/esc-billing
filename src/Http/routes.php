<?php

Route::group(['prefix' => 'process_billing'], function() {
    Route::get('after_purchase', '\Esc\Billing\Http\Controllers\ConfirmationController@confirmPurchase');
    Route::get('after_plan', '\Esc\Billing\Http\Controllers\ConfirmationController@confirmSubscription');
});