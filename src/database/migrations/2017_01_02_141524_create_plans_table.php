<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('plans', function($table) {
            $table->increments('id');
            $table->timestamps();
           
            $table->string('name');
            $table->integer('amount');
            
            $table->integer('trial_days')->default(0);
            $table->enum('trial_type', [
                'disabled',
                'account',
                'plan',
                'always'
            ])->default('disabled');
            
            $table->longtext('permissions');
            
            $table->string('terms')->nullable();
            $table->integer('capped_amount')->default(0);
            
            $table->integer('tokens_per_month')->default(0);
            $table->enum('refill_mode', [
                'never',
                'startofmonth',
                'standard'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
