<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrialPeriodToSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('subscriptions', function($table) {
            $table->timestamp('trial_end')->nullable();
            $table->boolean('had_trial')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('subscriptions', function($table) {
            $table->dropColumn(['trial_end', 'had_trial']);
        });
    }
}
