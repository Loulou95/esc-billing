<?php
namespace Esc\Billing;

use Illuminate\Database\Eloquent\Model;

use Esc\Billing\Plan;
use Esc\Billing\Discount;


class Subscription extends Model {
    protected $table = 'subscriptions';

    public function plan() {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public function shop() {
        return $this->belongsTo(\App\Shop::class, 'shop_id');
    }

    public function discount() {
        return $this->belongsTo(Discount::class, 'discount_id');
    }

    public function applyDiscountCode($code) {
        $error = false;
        $newPrice = \Esc\Billing\Discount::tryUse($code, $this->plan->amount, $error, 'subscription', $this->shop);
        if ($error) {
            return $error;
        }
        $this->amount_overrride = $newPrice;
        $this->discount()->associate(\Esc\Billing\Discount::findByCode($code));
        $this->save();
        return true;
    }



    private function determineTrialDays() {
        switch ($this->plan->trial_type) {
            case Plan::TRIAL_TYPE_DISABLED:
                return 0;
            case Plan::TRIAL_TYPE_EVERY_TIME:
                return $this->plan->trial_days;
            case Plan::TRIAL_TYPE_PER_PLAN:
                $sub = $this->shop->subscriptions()->where('plan_id', $this->plan->id)
                             ->whereNotNull('shopify_charge_id')->first();
                if ($sub) {
                    return 0;
                }
                return $this->plan->trial_days;
            case Plan::TRIAL_TYPE_PER_ACCOUNT:
                $sub = $this->shop->subscriptions()->whereNotNull('shopify_charge_id')->first();
                if ($sub) {
                    return 0;
                }
                return $this->plan->trial_days;
        }
        return 0;
    }


    public function createInShopify() {
        $trial = $this->determineTrialDays();
        try {
            $api = $this->shop->getAPI();
            $res = null;
            if ($this->plan->capped_amount > 0) {
                $res = $api->call('post', '/admin/recurring_application_charges.json', [
                    'recurring_application_charge' => [
                        'name' => $this->plan->name,
                        'price' => $this->plan->amount / 100,
                        //'price' => ($this->amount_overrride != -1 ? $this->amount_overrride : $this->plan->amount) / 100,
                        'return_url' => \URL::action('\Esc\Billing\Http\Controllers\ConfirmationController@confirmSubscription'),
                        'test' => $this->test,
                        'trial_days' => $trial,
                        //'capped_amount' => $this->plan->capped_amount / 100,
                        //'terms' => $this->plan->terms,
                    ]
                ]);
            } else {
                $res = $api->call('post', '/admin/recurring_application_charges.json', [
                    'recurring_application_charge' => [
                        'name' => $this->plan->name,
                        //'price' => ($this->amount_overrride != -1 ? $this->amount_overrride : $this->plan->amount) / 100,
                        'price' => $this->plan->amount / 100,
                        'return_url' => \URL::action('\Esc\Billing\Http\Controllers\ConfirmationController@confirmSubscription'),
                        'test' => $this->test,
                        'trial_days' => $trial,
                        //'terms' => $this->plan->terms,
                    ]
                ]);
            }

            $charge = $res->recurring_application_charge;
            if (!$charge || $charge->status != 'pending') {
                return false;
            }

            $this->shopify_charge_id = $charge->id;
            $this->status = $charge->status;
            $this->confirmation_url = $charge->confirmation_url;
            $this->save();

            return true;

        } catch (\Exception $ex) {
            throw $ex;
            return false;
        }
    }

    public function process() {
        try {
            $api = $this->shop->getAPI();
            $res = $api->call('get', '/admin/recurring_application_charges/'.$this->shopify_charge_id.'.json');
            $charge = $res->recurring_application_charge;

            $this->status = $charge->status;

            if ($this->status == 'accepted') {
                $res = $api->call('post', '/admin/recurring_application_charges/'.$this->shopify_charge_id.'/activate.json', [
                    'recurring_application_charge' => $charge
                ]);
                $charge = $res->recurring_application_charge;

                $this->status = $charge->status;
                $this->had_trial = $charge->trial_days > 0;
                $this->trial_end = $charge->trial_ends_on;

                if ($this->status == 'active') {

                    foreach ($this->shop->subscriptions()->where('status', 'active')->get() as $sub) {
                        $sub->status = 'cancelled';
                        $sub->active = false;
                        $sub->save();
                    }

                    $this->active = true;
                    $this->setting_up = false;
                    $this->shop->plan()->associate($this->plan);
                    $this->shop->tokens = $this->plan->tokens_per_month;
                    $this->shop->next_token_refresh = new \Carbon\Carbon('+30 days');
                    $this->shop->usage = 0;
                    $this->shop->next_usage_refresh = new \Carbon\Carbon('+30 days');
                    $this->shop->save();

                    $this->save();
                    return redirect()->to($this->redirect)->with('success', 'Your plan has been updated.');
                }
            }
            $this->save();
            return redirect()->to($this->redirect)->with('error', 'Plan switch was cancelled.');
        } catch (\Exception $ex) {
            throw $ex;
            return redirect()->to($this->redirect)->with('error', 'An error occurred.');
        }
    }

    public function checkActive() {
        try {
            $api = $this->shop->getAPI();
            $res = $api->call('get', '/admin/recurring_application_charges/'.$this->shopify_charge_id.'.json');
            $charge = $res->recurring_application_charge;

            $this->status = $charge->status;
            if ($charge->status != 'active') {
                $this->active = false;
                $this->save();

                $this->shop->plan_id = null;
                $this->shop->save();
                return false;
            }

            return true;

        } catch (\Exception $ex) {
            $this->status = 'frozen';
            $this->save();

            $this->shop->plan_id = null;
            $this->shop->save();
            return false;
        }
    }

    public function checkFrozen() {
        try {
            $api = $this->shop->getAPI();
            $res = $api->call('get', '/admin/recurring_application_charges/'.$this->shopify_charge_id.'.json');
            $charge = $res->recurring_application_charge;

            $this->status = $charge->status;
            if ($charge->status != 'active') {
                $this->active = false;
                $this->save();

                $this->shop->plan_id = null;
                $this->shop->save();
                return false;
            } else {
                $this->active = true;
                $this->save();
                $this->shop->plan()->associate($this->plan);
                $this->shop->save();
                return true;
            }

            return false;

        } catch (\Exception $ex) {
            $this->status = 'frozen';
            $this->save();

            $this->shop->plan_id = null;
            $this->shop->save();
            return false;
        }
    }

    public function redirect() {
        return \Response::make('<!DOCTYPE html><html><head></head><body><script type="text/javascript"> if (window.top == window.self) { window.top.location.href = "'.$this->confirmation_url.'"; } else { message = JSON.stringify({ message: "Shopify.API.remoteRedirect", data: { location: "'.$this->confirmation_url.'" } }); window.parent.postMessage(message, "https://'.$this->shop->shop_domain.'"); } </script></body></html>');
    }
}
