<?php
namespace Esc\Billing;

use Illuminate\Database\Eloquent\Model;


class TokenChange extends Model {
    protected $table = 'token_changes';
    
    public function shop() {
        return $this->belongsTo(\App\Shop::class, 'shop_id');
    }
}