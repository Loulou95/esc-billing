<?php

namespace Esc\Billing\Console\Commands;

use Illuminate\Console\Command;

class CheckActivePlans extends Command
{
    protected $signature = 'esc_billing:check_active';
    protected $description = '';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        foreach (\App\Shop::all() as $shop) {
            $sub = $shop->subscriptions()->where('active', true)->where('setting_up', false)->first();
            if (!$sub) { continue; }
            
            \Log::info('[Esc\Billing][CheckActivePlans] Checking plan for '.$shop->shop_domain);
            $api = $shop->getAPI();
            
            if ($sub->status == 'active') {
                if (!$sub->checkActive()) {
                    \Log::info('[Esc\Billing][CheckActivePlans] Disabling plan for '.$shop->shop_domain);
                }
            } else if ($sub->status == 'frozen') {
                if ($sub->checkFrozen()) {
                    \Log::info('[Esc\Billing][CheckActivePlans] Enabling plan for '.$shop->shop_domain);
                }
            }
        }
    }
}
