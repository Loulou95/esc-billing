<?php

namespace Esc\Billing\Console\Commands;

use Illuminate\Console\Command;

class UpdateUsages extends Command
{
    protected $signature = 'esc_billing:update_usages';
    protected $description = '';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        foreach (\App\Shop::all() as $shop) {
            if ($shop->plan && $shop->plan->tokens_per_month) {
                if (with(new \Carbon\Carbon($shop->next_token_refresh))->lt(new \Carbon\Carbon)) {
                    $shop->tokens = $shop->plan->tokens_per_month;
                    $shop->next_token_refresh = new \Carbon\Carbon('+30 days');
                    $shop->save();
                }
            } else {
                $shop->tokens = 0;
                $shop->save();
            }
            
            
            if ($shop->plan && $shop->plan->capped_amount) {
                if (with(new \Carbon\Carbon($shop->next_usage_refresh))->lt(new \Carbon\Carbon)) {
                    $shop->usage = 0;
                    $shop->next_usage_refresh = new \Carbon\Carbon('+30 days');
                    $shop->save();
                }
            } else {
                $shop->usage = 0;
                $shop->save();
            }
        }
    }
}
