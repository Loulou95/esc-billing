<?php
namespace Esc\Billing;

use Esc\Billing\Purchase;
use Esc\Billing\Plan;
use Esc\Billing\Subscription;

trait Billable {
    public function purchases() {
        return $this->hasMany(Purchase::class, 'shop_id');
    }

    public function subscriptions() {
        return $this->hasMany(Subscription::class, 'shop_id');
    }

    public function plan() {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

    public function tokenChanges() {
        return $this->hasMany(TokenChange::class, 'shop_id');
    }



    public function onTrial() {
        $sub = $this->subscriptions()->where('active', true)->first();
        if (!$sub) { return false; }
        return $sub->had_trial && with(new \Carbon\Carbon($sub->trial_end))->gt(new \Carbon\Carbon);
    }


    public function getLastPurchase() {
        $id = app(\Illuminate\Http\Request::class)->get('purchase_id');
        $purchase = Purchase::find($id);
        return $purchase;
    }

    public function createPurchase($amount, $name, $redirect, $test = false, $discountCode = null, &$error) {
        $purchase = new Purchase;
        $purchase->amount = $amount;
        $purchase->name = $name;
        $purchase->redirect = $redirect;
        $purchase->test = $test;
        $purchase->shop()->associate($this);
        $purchase->save();

        if (($msg = $purchase->applyDiscountCode($discountCode)) !== true) {
            $error = $msg;
            $purchase->delete();
            return false;
        }

        if (!$purchase->createInShopify()) {
            throw new \Exception('Could not create new purchase.');
        }

        $purchase->save();
        if (strpos($redirect, '?') === false) {
            $redirect .= '?purchase_id='.$purchase->id;
        } else {
            $redirect .= '&purchase_id='.$purchase->id;
        }
        $purchase->redirect = $redirect;
        $purchase->save();

        return $purchase;
    }


    public function hasPermission($permissions) {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        $plan = $this->plan;
        if (!$plan) { return false; }

        foreach ($permissions as $p) {
            if (!in_array($p, $plan->permissions)) {
                return false;
            }
        }
        return true;
    }


    public function switchPlan(Plan $plan, $redirect, $test = false, $discountCode = null, &$error) {
        $sub = new Subscription;
        $sub->plan()->associate($plan);
        $sub->shop()->associate($this);
        $sub->setting_up = true;
        $sub->active = false;
        $sub->test = $test;
        $sub->redirect = $redirect;
        $sub->save();

        if ($discountCode) {
            if (($msg = $sub->applyDiscountCode($discountCode)) !== true) {
                $error = $msg;
                $sub->delete();
                return false;
            }
        }

        if (!$sub->createInShopify()) {
            throw new \Exception('Could not create new subscription.');
        }

        $sub->save();

        return $sub;
    }

    public function getUsage() {
        return $this->usage;
    }

    public function recordUsage($amount, $reason) {
        if (!$this->plan || $this->plan->capped_amount < $amount) {
            return false;
        }

        try {
            $api = $this->getAPI();
            $chargeId = $this->subscriptions()->where('active', true)->first()->shopify_charge_id;
            $res = $api->call('post', '/admin/recurring_application_charges/'.$chargeId.'/usage_charges.json', [
                'usage_charge' => [
                    'description' => $reason,
                    'price' => $amount / 100
                ],
            ]);
            $charge = $res->usage_charge;

            $this->usage = intval(ceil($charge->balance_used * 100));
            $this->save();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function getTokens() {
        return $this->tokens;
    }

    public function addTokens($amount, $reason) {
        $this->tokens += $amount;
        $this->save();

        $change = new \Esc\Billing\TokenChange;
        $change->shop()->associate($this);
        $change->amount = $amount;
        $change->reason = $reason;
        $change->save();

        return $change;
    }

    public function consumeTokens($amount, $reason, $allowNegative = false) {
        if (!$allowNegative && $this->tokens < $amount) {
            return false;
        }

        $this->tokens -= $amount;
        $this->save();

        $change = new \Esc\Billing\TokenChange;
        $change->shop()->associate($this);
        $change->amount = -$amount;
        $change->reason = $reason;
        $change->save();

        return $change;
    }

    public function getTokenLimit() {
        return $this->plan->tokens_per_month;
    }

    public function getUsageLimit() {
        return $this->plan->capped_amount;
    }
}
